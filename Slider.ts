class Slider {
    private readonly input: JQuery;
    private readonly parent: JQuery;
    private readonly holder: JQuery;
    private readonly leftHead: JQuery;
    private readonly rightHead: JQuery;
    private readonly body: JQuery;
    private readonly head: JQuery;
    private static headsClass: string[] = ['slider-head', 'slider-left-head', 'slider-right-head'];
    private whichHeadMustMove: number = 0;
    private readonly range: number[];
    private readonly prettify = (x, y) => {
        return [x, y]
    };
    private readonly onChange = (x, y) => {
        return x+y
    }
    private readonly onStart = (x, y) => {
        return x+y

    }
    private readonly onUpdate = (x, y) => {
        return x+y

    }

    setBubbles(data: number[]) {
        this.leftHead.find('.bubble').text(data[0]);
        this.rightHead.find('.bubble').text(data[1]);
    }

    private setNumbers() {
        let setPercent = (head: JQuery) => {
            return Math.round((Math.abs(head.offset().left - this.holder.offset().left) / (this.holder.width())) * (this.range[1] - this.range[0]) + this.range[0])
        }
        return [setPercent(this.leftHead), setPercent(this.rightHead)];
    }

    private getNumbersForBubble() {
        let data = this.setNumbers();
        this.setBubbles(this.prettify(data[0], data[1]));
        this.onChange(data[0], data[1]);
    }

    private headMoved() {
        if (this.whichHeadMustMove !== 0) {
            let data = this.setNumbers();
            this.onUpdate(data[0], data[1]);
            this.getNumbersForBubble();
        }
        this.whichHeadMustMove = 0;

    }

    private moveBody() {
        let headWidth: number = 2;
        let leftHeadLeft = parseInt(this.leftHead.css('left')) + headWidth;
        let rightHeadLeft = parseInt(this.rightHead.css('left'));
        this.body.css('left', (leftHeadLeft / this.holder.width() * 100).toString() + '%');
        this.body.css('width', ((rightHeadLeft - leftHeadLeft + headWidth - 1) / this.holder.width() * 100).toString() + '%');
    }

    private setHeadPosWithClick(ox: number, whichHead = 0) {
        let holderOffsetLeft: number = this.holder.offset().left;
        ox = ox - holderOffsetLeft;
        let distances: number[] = [Math.abs(this.leftHead.offset().left - ox), Math.abs(this.rightHead.offset().left - ox)];
        let pxPercent: string = (ox / (this.holder.width()) * 100).toString() + '%';
        if (whichHead === 0) {
            if (distances[0] < distances[1])
                this.leftHead.css('left', pxPercent)
            else
                this.rightHead.css('left', pxPercent)
        } else if (whichHead === 1) {
            if (this.rightHead.offset().left >= ox + this.holder.offset().left)
                this.leftHead.css('left', pxPercent)
        } else {
            if (this.leftHead.offset().left <= ox + this.holder.offset().left)
                this.rightHead.css('left', pxPercent)
        }
        this.moveBody();
        this.getNumbersForBubble();
        let data = this.setNumbers();
        this.onUpdate(data[0], data[1]);
    }

    private setHeadPosWithDrag(px) {
        let holderOffsetLeft: number = this.holder.offset().left;
        if (this.whichHeadMustMove != 0 && (Math.floor(px) <= Math.floor(holderOffsetLeft) || Math.floor(holderOffsetLeft + this.holder.width()) <= Math.floor(px))) {
            this.headMoved();
            return
        }
        px = px - holderOffsetLeft;
        let pxPercent: string = (px / (this.holder.width()) * 100).toString() + '%';
        if (this.whichHeadMustMove === 1 && px <= this.rightHead.offset().left - holderOffsetLeft) {
            this.leftHead.css('left', pxPercent);
            this.getNumbersForBubble();
        } else if (this.whichHeadMustMove === 2 && px >= this.leftHead.offset().left - holderOffsetLeft) {
            this.rightHead.css('left', pxPercent);
            this.getNumbersForBubble();
        }
    }

    constructor(option: object) {
        let _self = this;
        if (option['prettify'] !== undefined)
            this.prettify = option['prettify'];
        if (option['onChange'] !== undefined)
            this.onChange = option['onChange'];
        if (option['onStart'] !== undefined)
            this.onStart = option['onStart'];
        if (option['onUpdate'] !== undefined)
            this.onUpdate = option['onUpdate'];
        this.range = option['range'];
        this.input = $(`#${option['input']}`);
        this.input.css('display', 'none');
        this.input.wrap('<div class="slider-parent"></div>');
        this.parent = this.input.parent();
        this.parent.append('<div class="slider-holder"><span class="legend"></span><span class="legend"></span><span class="legend"></span><span class="legend"></span><span class="legend"></span></div>');
        this.holder = this.parent.find('.slider-holder');
        this.holder.append('<div class="slider-head slider-left-head"><div class="bubble"></div></div><div class="slider-body"></div><div class="slider-head slider-right-head"><div class="bubble"></div></div>');
        this.leftHead = this.holder.find('.slider-left-head');
        this.rightHead = this.holder.find('.slider-right-head');
        this.rightHead.css('left', '50%');
        this.body = this.holder.find('.slider-body');
        this.head = this.holder.find('.slider-head');
        this.moveBody();
        this.head.on('mousedown', function () {
            _self.whichHeadMustMove = $(this).hasClass(Slider.headsClass[1]) ? 1 : 2;
        })

        $('html').on('mousemove', function (e) {
            _self.setHeadPosWithDrag(e.pageX);
            _self.moveBody();
        }).on('mouseup', function () {
            _self.headMoved();
        });
        this.holder.on('click', function (e) {
            if (!$(e.target).hasClass(Slider.headsClass[0])) {
                _self.setHeadPosWithClick(e.pageX);
            }
        });
        let data = this.setNumbers();
        let i = 0, number;
        this.holder.find('.legend').each(function () {
            number = Math.round(i * (_self.range[1] - _self.range[0]) / 100) + _self.range[0];
            $(this).append(`|<br><span>${_self.prettify(number, 1)[0]}</span>`);
            i += 25;
        });
        let initialsValues = option['initialValues'];
        this.setHeadPosWithClick(this.setFromToForPos(initialsValues[0]), 1);
        this.setHeadPosWithClick(this.setFromToForPos(initialsValues[1]), 2);
        this.onStart(data[0], data[1]);
    }

    private setFromToForPos(num) {
        return (parseInt(num) - this.range[0]) / (this.range[1] - this.range[0]) * (this.holder.width()) + this.holder.offset().left
    }

    public setSliderChanger(params) {
        let _self = this;
        let from, to;
        if (params['from'] != undefined) {
            from = _self.setFromToForPos((params['from']));
            if (from >= 0)
                _self.setHeadPosWithClick(from, 1)
        }
        if (params['to'] != undefined) {
            to = _self.setFromToForPos((params['to']));
            if (to <= 1)
                _self.setHeadPosWithClick(to, 2)
        }
    }
}