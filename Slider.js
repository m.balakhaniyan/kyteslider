var Slider = /** @class */ (function () {
    function Slider(option) {
        this.whichHeadMustMove = 0;
        this.prettify = function (x, y) {
            return [x, y];
        };
        this.onChange = function (x, y) {
        };
        this.onStart = function (x, y) {
        };
        this.onUpdate = function (x, y) {
        };
        var _self = this;
        if (option['prettify'] !== undefined)
            this.prettify = option['prettify'];
        if (option['onChange'] !== undefined)
            this.onChange = option['onChange'];
        if (option['onStart'] !== undefined)
            this.onStart = option['onStart'];
        if (option['onUpdate'] !== undefined)
            this.onUpdate = option['onUpdate'];
        this.range = option['range'];
        this.input = $("#" + option['input']);
        this.input.css('display', 'none');
        this.input.wrap('<div class="slider-parent"></div>');
        this.parent = this.input.parent();
        this.parent.append('<div class="slider-holder"><span class="legend">1</span><span class="legend">1</span><span class="legend">1</span><span class="legend">1</span><span class="legend">1</span></div>');
        this.holder = this.parent.find('.slider-holder');
        this.holder.append('<div class="slider-head slider-left-head"><div class="bubble"></div></div><div class="slider-body"></div><div class="slider-head slider-right-head"><div class="bubble"></div></div>');
        this.leftHead = this.holder.find('.slider-left-head');
        this.rightHead = this.holder.find('.slider-right-head');
        this.rightHead.css('left', '50%');
        this.body = this.holder.find('.slider-body');
        this.head = this.holder.find('.slider-head');
        this.moveBody();
        this.head.on('mousedown', function () {
            _self.whichHeadMustMove = $(this).hasClass(Slider.headsClass[1]) ? 1 : 2;
        });
        $('html').on('mousemove', function (e) {
            _self.setHeadPosWithDrag(e.pageX);
            _self.moveBody();
        }).on('mouseup', function () {
            _self.headMoved();
        });
        this.holder.on('click', function (e) {
            if (!$(e.target).hasClass(Slider.headsClass[0])) {
                _self.setHeadPosWithClick(e.pageX);
            }
        });
        var data = this.setNumbers();
        var i = 0, number;
        this.holder.find('.legend').each(function () {
            number = Math.round(i * (_self.range[1] - _self.range[0]) / 100) + _self.range[0];
            $(this).text(_self.prettify(number, 1)[0]);
            i += 25;
        });
        var initialsValues = option['initialValues'];
        this.setHeadPosWithClick(this.setFromToForPos(initialsValues[0]), 1);
        this.setHeadPosWithClick(this.setFromToForPos(initialsValues[1]), 2);
        this.onStart(data[0], data[1]);
    }
    Slider.prototype.setBubbles = function (data) {
        this.leftHead.find('.bubble').text(data[0]);
        this.rightHead.find('.bubble').text(data[1]);
    };
    Slider.prototype.setNumbers = function () {
        var _this = this;
        var setPercent = function (head) {
            return Math.round((Math.abs(head.offset().left - _this.holder.offset().left) / (_this.holder.width())) * (_this.range[1] - _this.range[0]) + _this.range[0]);
        };
        var data = [setPercent(this.leftHead), setPercent(this.rightHead)];
        return this.prettify(data[0], data[1]);
    };
    Slider.prototype.getNumbersForBubble = function () {
        var data = this.setNumbers();
        this.setBubbles(data);
        this.onChange(data[0], data[1]);
    };
    Slider.prototype.headMoved = function () {
        this.whichHeadMustMove = 0;
        var data = this.setNumbers();
        this.onUpdate(data[0], data[1]);
        this.getNumbersForBubble();
    };
    Slider.prototype.moveBody = function () {
        var headWidth = 2;
        var leftHeadLeft = parseInt(this.leftHead.css('left')) + headWidth;
        var rightHeadLeft = parseInt(this.rightHead.css('left'));
        this.body.css('left', (leftHeadLeft / this.holder.width() * 100).toString() + '%');
        this.body.css('width', ((rightHeadLeft - leftHeadLeft + headWidth - 1) / this.holder.width() * 100).toString() + '%');
        // this.getNumbersForBubble();
    };
    Slider.prototype.setHeadPosWithClick = function (ox, whichHead) {
        if (whichHead === void 0) { whichHead = 0; }
        var holderOffsetLeft = this.holder.offset().left;
        ox = ox - holderOffsetLeft;
        var distances = [Math.abs(this.leftHead.offset().left - ox), Math.abs(this.rightHead.offset().left - ox)];
        var pxPercent = (ox / (this.holder.width()) * 100).toString() + '%';
        if (whichHead === 0) {
            if (distances[0] < distances[1])
                this.leftHead.css('left', pxPercent);
            else
                this.rightHead.css('left', pxPercent);
        }
        else if (whichHead === 1) {
            if (this.rightHead.offset().left >= ox + this.holder.offset().left)
                this.leftHead.css('left', pxPercent);
        }
        else {
            if (this.leftHead.offset().left <= ox + this.holder.offset().left)
                this.rightHead.css('left', pxPercent);
        }
        this.moveBody();
        this.getNumbersForBubble();
        var data = this.setNumbers();
        this.onUpdate(data[0], data[1]);
    };
    Slider.prototype.setHeadPosWithDrag = function (px) {
        var holderOffsetLeft = this.holder.offset().left;
        if (this.whichHeadMustMove !== 0 && (Math.floor(px) <= Math.floor(holderOffsetLeft) || Math.floor(holderOffsetLeft + this.holder.width()) <= Math.floor(px))) {
            this.headMoved();
            return;
        }
        px = px - holderOffsetLeft;
        var pxPercent = (px / (this.holder.width()) * 100).toString() + '%';
        if (this.whichHeadMustMove === 1 && px <= this.rightHead.offset().left - holderOffsetLeft) {
            this.leftHead.css('left', pxPercent);
        }
        else if (this.whichHeadMustMove === 2 && px >= this.leftHead.offset().left - holderOffsetLeft) {
            this.rightHead.css('left', pxPercent);
        }
        this.getNumbersForBubble();
    };
    Slider.prototype.setFromToForPos = function (num) {
        return (parseInt(num) - this.range[0]) / (this.range[1] - this.range[0]) * (this.holder.width()) + this.holder.offset().left;
    };
    Slider.prototype.setSliderChanger = function (params) {
        var _self = this;
        var from, to;
        if (params['from'] !== undefined) {
            from = _self.setFromToForPos((params['from']));
            if (from >= 0)
                _self.setHeadPosWithClick(from, 1);
        }
        if (params['to'] !== undefined) {
            to = _self.setFromToForPos((params['to']));
            if (to <= 1)
                _self.setHeadPosWithClick(to, 2);
        }
    };
    Slider.headsClass = ['slider-head', 'slider-left-head', 'slider-right-head'];
    return Slider;
}());
var slider = new Slider({
    input: 'inp',
    range: [0, 100],
    initialValues: [20, 25],
    prettify: function (x, y) {
        return [x + 100, y + 100];
    },
});
$('#main').on('input', function () {
    slider.setSliderChanger({
        from: $('#ii').val()
    });
});
//# sourceMappingURL=Slider.js.map